var getData = require('./getData');

describe('getData', () => {
    it('Contains user "joe"', () => {
        var myCallback = function(data) {
            expect(data).toMatchSnapshot();
        };

        getData("joe", myCallback);
    });

    it('Missing user "Mart"', () => {
        var myCallback = function(data) {
            expect(data).toMatchSnapshot();
        };

        getData("Mart", myCallback);
    });
});
  